// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_CLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <pthread.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DatosMemCompartida.h"
//#include "Esfera.h"
//#include "Raqueta.h"
#include "Socket.h"


class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnTimer2(int value);
	void OnDraw();	
	
	char * path_f_proy;
	char * path_fifo_servidor;
	char * path_fifo_teclas;
	
	Esfera esferaJ1,esferaJ2;
	
	std::vector<Esfera *> listaEsferas;
	std::vector<Plano> paredes;
	
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	
	int fd_tecla;	 //Descriptor de la FIFO para envío de teclas pulsadas.
	int fd_servidor; //Descriptor de fichero del FIFO cliente-servidor
	int fd_bot;	//Descriptor del fichero que se proyecta
	
	int contJ1;
	int contJ2;
	int datos1[2]; // [0]-> Número del jugador [1]-> Puntos del jugador
	int datos2[2];
	
	DatosMemCompartida datosIA;
	DatosMemCompartida * p_datosIA;
	struct stat bstat;

	Socket socket_comunicacion;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
