#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

int fd_logger;
char * path_fifo = "/tmp/logger";

int cerrarCorrectamente();

//Manejador
void handlerLogger (int sig){

	if (sig == SIGKILL){
		cerrarCorrectamente();
		exit(1);
    	}
	if (sig == SIGINT){
		cerrarCorrectamente();
		exit(1);
    	}
	if (sig == SIGTERM){
		cerrarCorrectamente();
		exit(1);
    	}
	if (sig == SIGPIPE){
		cerrarCorrectamente();
		exit(1);
    	}
	if (sig == SIGUSR1){
		cerrarCorrectamente();
		exit(0);
    	}
}

//Main
int main(){

  
  int datos[2];
  int dato_leido;
  printf ("Logger en ejecución...\n");

//Tratamiento de señales
  struct sigaction act;
  act.sa_handler = handlerLogger;
  act.sa_flags=0;
  sigemptyset(&act.sa_mask);
  sigaction(SIGINT,&act,NULL);
  sigaction(SIGTERM,&act,NULL);
  sigaction(SIGPIPE,&act,NULL);
  sigaction(SIGUSR1,&act,NULL);
  
  if( mkfifo(path_fifo, (mode_t) S_IRWXU ) < 0){
	perror("**Error creating FIFO");
	return -1;
  }
  else {
    printf("--FIFO logger creada.\n");
  }

  if ((fd_logger = open(path_fifo, O_RDONLY)) < 0){
    perror("**Error openning FIFO");
    return -1;
  }  
  printf ("--FIFO abierta en modo lectura.\n");
  while(1){
    dato_leido = read(fd_logger, datos, sizeof(datos));
    if(dato_leido < 0){
	perror("Error reading logger FIFO");
	break;
    }
    else{
      if(dato_leido > 0)
	printf("Jugador %d marca 1 punto, lleva un total de %d puntos\n", datos[0], datos[1]);
    }
  }
  cerrarCorrectamente();
  printf("-->Logger finalizado\n");
  return 0;
} 

//Cerrar
int cerrarCorrectamente(){

  if ( close(fd_logger) < 0){
	perror("Error closing FIFO");
	return -1;
  }
  unlink(path_fifo);
  return 0;
}

