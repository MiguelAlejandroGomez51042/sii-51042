#include "DatosMemCompartida.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdlib.h>


int main(){
  
      float y_raqueta;
      void * p_void;
      DatosMemCompartida * p_datosIA;
      int fd;
      struct stat bstat;
  
      if( (fd = open("/tmp/bot", O_RDWR, (mode_t) 0777)) < 0){
	  perror("Error openning the file");
	  close(fd);
	  return -1;
      }
 
      if (fstat(fd, &bstat) < 0) {
	  perror("Wrong fstat of file");
	  close(fd);
	  return -1;
      }
  
      p_void =  mmap( (caddr_t) 0,bstat.st_size , PROT_READ | PROT_WRITE, MAP_SHARED , fd, 0 );
      close(fd);
      
      if(p_void == MAP_FAILED){
	  perror("Map failed");
	  return -1;    
      }
  
      p_datosIA = static_cast <DatosMemCompartida*> (p_void);
  
      while(1){
	  y_raqueta = (float) (p_datosIA->raqueta1.y2-p_datosIA->raqueta1.y1)/2.0f+p_datosIA->raqueta1.y1;
   
	  float distancia_horizontal=abs(p_datosIA->raqueta1.x2 - p_datosIA->esfera.centro.x);
	  float diferencia_alturas= y_raqueta - p_datosIA->esfera.centro.y;
    
	  p_datosIA->accion = 0;
	  
	  if(distancia_horizontal <= 5.0f){
    	      if( diferencia_alturas <= -0.30000f)
		  p_datosIA->accion = 1;
	      else if( diferencia_alturas >= 0.30000f)
		  p_datosIA->accion = -1;
	  }    
	  usleep(25000);
      }
      return 0;
}
