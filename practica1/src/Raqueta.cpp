// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	velocidad.x=velocidad.y=0.0f;
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	x1+=velocidad.x*t;
	x2+=velocidad.x*t;
	y1+=velocidad.y*t;
	y2+=velocidad.y*t;
}
