// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#define POINTS_TO_WIN 100

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	for(unsigned i=0;i<listaEsferas.size();i++){
	  delete listaEsferas[i];
	}
	
	if( munmap(p_datosIA, bstat.st_size) < 0){
	  perror("Error un-mmapping the file");
	} 
	close(fd_bot);
	unlink(path_f_proy);
/**************************************************** FIFOs
	if( close(fd_servidor) < 0){
	      perror("Error closing server-client FIFO");
	}
	unlink(path_fifo_servidor);
	
	if( close(fd_tecla) < 0){
	      perror("Error closing keys FIFO");
	}
	unlink(path_fifo_teclas);
	printf("-->Cliente finalizado.\n");
*****************************************************************/
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	
//Identificador de cliente que aparece en pantalla.
	char id[100];
	sprintf(id,"Cliente");
	print(id,340,0,1,1,1);
////////////////////////////////////////////////////
	
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",datos1[1]);
	print(cad,10,0,1,1,1);
	
	sprintf(cad,"Jugador2: %d",datos2[1]);
	print(cad,650,0,1,1,1);
	for(int i=0;i<paredes.size();i++){
		paredes[i].Dibuja();
	}
	for(int j=0; j<listaEsferas.size(); j++)
	  listaEsferas[j]->Dibuja();
	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	
	if(esferaJ1.radio == 0.25f)esferaJ1.Dibuja();
	if(esferaJ2.radio == 0.25f)esferaJ2.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	char cadena_cliente[400];
	//char cad_esf[200];

	
//Aquí leo del FIFO servidor
	int cadena_leida;
	/***************************************
	cadena_leida = read(fd_servidor, cadena_cliente, sizeof(cadena_cliente));
	if( cadena_leida < 0 ){
	    close(fd_servidor);
	    perror("Error reading server FIFO");
	}
	//else printf("Cadena leída de la FIFO.\nDato centro esfera = ( %f, %f)\n",cadena_cliente[0], cadena_cliente[1]);
	
//Aquí leo de la cadena
	sscanf(cadena_cliente,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %d %d ", 
		&esferaJ1.centro.x , &esferaJ1.centro.y, &esferaJ1.radio, &esferaJ2.centro.x, &esferaJ2.centro.y, &esferaJ2.radio, 
			&(listaEsferas[0]->radio), &(listaEsferas[0]->centro.x),
 		&(listaEsferas[0]->centro.y), &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,
			&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &datos1[1], &datos2[1]
	);
	**************************************************************/

	char mensaje[500];
	socket_comunicacion.Receive(mensaje,sizeof(mensaje));
	sscanf(mensaje,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %d %d ", 
		&esferaJ1.centro.x , &esferaJ1.centro.y, &esferaJ1.radio, &esferaJ2.centro.x, &esferaJ2.centro.y, &esferaJ2.radio, 
			&(listaEsferas[0]->radio), &(listaEsferas[0]->centro.x),
 		&(listaEsferas[0]->centro.y), &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,
			&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &datos1[1], &datos2[1]
	);
	




/********************Lectura con varias esferas*************************************************************************	
    //Lectura de los datos comunes
	sscanf(cadena_cliente,"%f %f %f %f %f %f %f %f %d %d %s",
	    &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,
	&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &datos1[1], &datos2[1], cad_esf
	);
	
    //Lectura de los datos de las esferas
	char * coord_x;
	char * coord_y;
	unsigned j=0;
	int cercano=0;
	
	coord_x = strtok (cad_esf,",");
	while (coord_x != NULL)
	{   
	    int num_esf = listaEsferas.size();
	    coord_y = strtok (NULL, ",");
	    if(coord_y == NULL) break;
	    if(j == num_esf) listaEsferas.push_back(new Esfera);
	    //printf("Número de esferas en la lista: %d\n j = %d\n", num_esf, j);
	    sscanf(coord_x,"%f", &(listaEsferas[j]->centro.x ));
	    sscanf(coord_y,"%f", &(listaEsferas[j]->centro.y ));
	    coord_x = strtok(NULL, ",");
	    j++;
	}
	//printf("Coordenada X de esfera[0]: %f\nCoordenada Y de esfera[0]: %f\n",listaEsferas[0]->centro.x,listaEsferas[0]->centro.y);

***************************************************************************************************************************/
	

//Datos para la IA
	/*for(unsigned j=0; j < listaEsferas.size(); j++){
	  if(listaEsferas[j]->centro.x < listaEsferas[cercano]->centro.x) 
			cercano=j;
	}*/

	datosIA.esfera.centro.x = listaEsferas[0]->centro.x;
	datosIA.esfera.centro.y = listaEsferas[0]->centro.y;
	datosIA.raqueta1 = jugador1;
	
	p_datosIA->esfera = (*listaEsferas[0]);
	p_datosIA->raqueta1=jugador1;
	
	if(p_datosIA->accion==1) OnKeyboardDown('w',1,1);
	if(p_datosIA->accion==-1) OnKeyboardDown('s',1,1);

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	char cad_tecl[100];
	
	/**************************************************
	fd_tecla = open(path_fifo_teclas , O_WRONLY);
	
	if( fd_tecla < 0 ){
	      close(fd_tecla);    
	      perror(" (Client) Error openning keys FIFO");
	}	
	
	if( write(fd_tecla, cad_tecl, sizeof(cad_tecl)) < 0){
		close(fd_tecla);
		perror("Error writing on keys FIFO");
	}
	//else printf("Tecla escrita en FIFO: %c\n",cad_tecl[0]);
*****************************************************************/
	sprintf(cad_tecl, "%c", key);	
	socket_comunicacion.Send(cad_tecl,sizeof(cad_tecl));
}

void CMundo::Init()
{
printf("Cliente en ejecución...\n");

///////////////////////////////////////////////
////////////PROYECCION DE FICHERO//////////////
///////////////////////////////////////////////
  
	void * p_void;
	path_f_proy = "/tmp/bot";
	char nombre[100];

	printf("Introduzca nombre: \n");
	scanf("%s",nombre);
	socket_comunicacion.Connect("127.0.0.1",4200); //COMPROBAR PUERTO
	socket_comunicacion.Send(nombre,sizeof(nombre));

//Proyección del fichero bot       
int error=0;
if( (fd_bot = open( path_f_proy, O_RDWR | O_CREAT | O_EXCL, (mode_t) 0777 ) ) < 0){	    
		perror("**Error openning bot file");
		close(fd_bot);
		error=1;
	}
	
	if( write(fd_bot, &datosIA, sizeof(datosIA) ) < 0){
	    perror("**Error writing on bot file");
	    close(fd_bot);
	    error=1;
	}
	
	if (fstat(fd_bot, &bstat)<0) {
	  perror("**Wrong fstat of file");
	  close(fd_bot);
	  error=1;
	}

	p_void = mmap(0, bstat.st_size, PROT_WRITE | PROT_READ, MAP_SHARED ,fd_bot, (off_t) 0);
	close(fd_bot);
	
	if(p_void == MAP_FAILED){
	  perror("**Map failed");
	  error=1;
	}
	
	if(error == 0)	p_datosIA = static_cast <DatosMemCompartida*> (p_void); 
	printf("Guay\n");
	
///////////////////////////////////////////////
  ///INICIALIZACIÓN DE LOS DATOS DE MUNDO///
///////////////////////////////////////////////
  
//datos del jugador y puntos
	datos1[0]=1;	 
	datos1[1]=0;
	datos2[0]=2;
	datos2[1]=0;
	Plano p;
	listaEsferas.push_back(new Esfera);

//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

//fondos dcho e izq
	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

//raqueta izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

//raqueta dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
