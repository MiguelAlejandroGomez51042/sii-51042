// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <signal.h>
#define POINTS_TO_WIN 100

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
 

//Manejador
void manejadorSenales (int sig){
/*
	if (sig == SIGKILL){
		exit(1);
    	}
	if (sig == SIGINT){
		exit(1);
    	}
	if (sig == SIGTERM){
		exit(1);
    	}
	if (sig == SIGPIPE){
		exit(1);
    	}*/
}

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	for(unsigned i=0;i<listaEsferas.size();i++){
	  delete listaEsferas[i];
	}
/*	if( close(fd_servidor) < 0){
	  perror("Error closing server FIFO");    
	}
	//unlink(path_fifo_servidor); 
*/
	
	if( close(fd_logger) < 0){
	  perror("Error closing logger FIFO");    
	}
	unlink(path_fifo);
	
/******************************************************
	if( close(fd_tecla) < 0){
	  perror("Error closing keys FIFO");
	}
	//unlink(path_fifo_teclas);
******************************************************/

	//socket_conexion.Close();
	socket_comunicacion_cliente.Close();
	for(int i=0;i<conexiones.size();i++)  
	    conexiones[i].Close();
	finalizar = true;
	printf("-->Servidor finalizado.\n");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char * mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	
	//Identificador de cliente que aparece en pantalla.
	char id[100];
	sprintf(id,"Servidor");
	print(id,340,0,1,1,1);
////////////////////////////////////////////////////
	
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",datos1[1]);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",datos2[1]);
	print(cad,650,0,1,1,1);
	for(int i=0;i<paredes.size();i++){
		paredes[i].Dibuja();
	}
	for(int j=0; j<listaEsferas.size(); j++)
	  listaEsferas[j]->Dibuja();
	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	
	if(esferaJ1.radio==0.25) esferaJ1.Dibuja();
	if(esferaJ2.radio==0.25) esferaJ2.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	    
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esferaJ1.Mueve(0.025f);
	esferaJ2.Mueve(-0.025f);


	if(jugador2.Rebota(esferaJ1)) {
		  jugador2.velocidad.x=0;
		  jugador2.velocidad.y=0;
		  esferaJ1.centro.y=20; //alejar juego
		  esferaJ1.radio=0.0f;
		  contJ2=30;
	}
	if(contJ2>0) {
		  contJ2--;
		  jugador2.velocidad.x=0;
		  jugador2.velocidad.y=0;
	}
	if(fondo_dcho.Rebota(esferaJ1))
		  esferaJ1.radio=0.0f;
	if(fondo_izq.Rebota(esferaJ2))
		  esferaJ2.radio=0.0f;
	if(jugador1.Rebota(esferaJ2)) {
		  jugador1.velocidad.x=0;
		  jugador1.velocidad.y=0;
		  esferaJ2.centro.y=20; //alejar juego
		  esferaJ2.radio=0.0f;
		  contJ1=30;
	  }
	  if(contJ1>0) {
		  contJ1--;
		  jugador1.velocidad.x=0;
		  jugador1.velocidad.y=0;
	  }
	  
	for(unsigned j=0;j<listaEsferas.size();j++)
	{
	  listaEsferas[j]->Mueve(0.025f);
	  if(listaEsferas[j]->radio>=0.1f)
		listaEsferas[j]->radio-=0.0001f;
	  
	  for(int i=0; i<paredes.size();i++)
	  {
		paredes[i].Rebota(*listaEsferas[j]);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	  }	

	  jugador1.Rebota(*listaEsferas[j]);
	  jugador2.Rebota(*listaEsferas[j]);
	  
	  if(fondo_izq.Rebota(*listaEsferas[j]))
	  {
		listaEsferas[j]->centro.x=0;
		listaEsferas[j]->centro.y=rand()/(float)RAND_MAX;
		listaEsferas[j]->velocidad.x=2+2*rand()/(float)RAND_MAX;
		listaEsferas[j]->velocidad.y=2+2*rand()/(float)RAND_MAX;
		datos2[1]++;
		write(fd_logger, datos2, sizeof(datos2));
	  }

	  if(fondo_dcho.Rebota(*listaEsferas[j]))
	  {
		listaEsferas[j]->centro.x=0;
		listaEsferas[j]->centro.y=rand()/(float)RAND_MAX;
		listaEsferas[j]->velocidad.x=-2-2*rand()/(float)RAND_MAX;
		listaEsferas[j]->velocidad.y=-2-2*rand()/(float)RAND_MAX;
		datos1[1]++;
		write(fd_logger, datos1, sizeof(datos1));
	  }
	}
	//////////////////////ENVIO DE COORDENADAS/////////////////////

	char mensaje[500];
	sprintf(mensaje,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %d %d ", 
	    esferaJ1.centro.x , esferaJ1.centro.y, esferaJ1.radio, esferaJ2.centro.x, esferaJ2.centro.y, esferaJ2.radio,
		listaEsferas[0]->radio, listaEsferas[0]->centro.x, listaEsferas[0]->centro.y,
	    jugador1.x1,jugador1.y1, jugador1.x2,jugador1.y2,
		jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, datos1[1], datos2[1]
	);
	//socket_comunicacion_cliente.Send(mensaje,sizeof(mensaje));


	//char cad[200];
	for (int i=conexiones.size()-1; i>=0; i--){
	    if ((conexiones[i].Send(mensaje,500)) <= 0){
		conexiones.erase(conexiones.begin()+i);
            	if (i < 2)// Hay menos de dos clientes conectados
		{
		  datos1[1]=0;	 // Se resetean los puntos a cero
		  datos2[1]=0;
		} 
	    }
	 }

	if(datos1[1] >= POINTS_TO_WIN){
	  printf("Fin del juego. Gana el jugador 1.\n");
	  exit(0);
	}
	if (datos2[1] >= POINTS_TO_WIN){
	  printf("Fin del juego. Gana el jugador 2.\n");
	  exit(0);
	}

/***********************Versión con varias esferas*****************************************************************
//Aquí se escribe en la cadena
	char cad[400];		//Almacena el conjunto de los datos.
	char cad_esf[200];	//Almacena los datos de las esferas.
	
	sprintf(cad,"%f %f %f %f %f %f %f %f %d %d",
	    jugador1.x1,jugador1.y1, jugador1.x2,jugador1.y2,
	 jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, datos1[1], datos2[1]
	);
	
	for(unsigned j=0;j<listaEsferas.size();j++) {
	    sprintf(cad_esf,"%s,%f,%f", cad_esf, 
		  listaEsferas[j]->centro.x,
		  listaEsferas[j]->centro.y
	    );
	}
	//printf("coordenada X de esfera[0]: %f\ncoordenada Y de esfera[0]: %f\n",listaEsferas[0]->centro.x,listaEsferas[0]->centro.y);
	
	
//Incluye los datos de esferas a la cadena principal.
	strncat(cad, cad_esf, sizeof(cad));

//Aquí se escribe en la cadena	
	char cad[400];		//Almacena el conjunto de los datos.
	
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %d %d ", 
	    esferaJ1.centro.x , esferaJ1.centro.y, esferaJ1.radio, esferaJ2.centro.x, esferaJ2.centro.y, esferaJ2.radio,
		listaEsferas[0]->radio, listaEsferas[0]->centro.x, listaEsferas[0]->centro.y,
	    jugador1.x1,jugador1.y1, jugador1.x2,jugador1.y2,
		jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, datos1[1], datos2[1]
	);

//Aquí se escribe en la FIFO

	if( write(fd_servidor, cad, sizeof(cad)) < 0){
		close(fd_servidor);
		//perror("Error writing on server FIFO");
	}
****************************************************************/
}

void CMundo::OnTimer2(int value)
{
}

void* hilo_comandosJ1(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador1();
}

void* hilo_comandosJ2(void* d)
{
   CMundo* p=(CMundo*) d;
   p->RecibeComandosJugador2();
}

void* hilo_conexiones(void *d)
{
      CMundo* p= (CMundo*) d;
      p->gestionaConexiones();
}

void CMundo::RecibeComandosJugador2()
{

     while (!finalizar) {
            usleep(10);
            char cad[100];
	    unsigned char key;
	    
	///////////RECEPCION COMANDOS////
	    
	//socket_comunicacion_cliente.Receive(cad,sizeof(cad));
	if(conexiones.size() > 0){
	  conexiones[0].Receive(cad, 100);
	  sscanf(cad,"%c", &key);
	  if(key=='l')jugador2.velocidad.y=-4;
          if(key=='o')jugador2.velocidad.y=4;
	  if (key== 'k'){
		esferaJ2.velocidad.y=0.025;
		esferaJ2.centro.x=6;
		esferaJ2.centro.y=((jugador2.y1+jugador2.y2)/2);
		esferaJ2.radio=0.25f;
	    }
	}
     }
}

void CMundo::RecibeComandosJugador1()
{
       while (!finalizar) {
            usleep(10);
            char cad[100];
	    unsigned char key;    
	    
	//socket_comunicacion_cliente.Receive(cad,sizeof(cad));
	if(conexiones.size() > 0){
	  conexiones[1].Receive(cad, 100);
	  sscanf(cad,"%c", &key);
	   if(key=='s')jugador1.velocidad.y=-4;
           if(key=='w')jugador1.velocidad.y=4;
	   if (key== 'q'){
		esferaJ1.velocidad.y=0.025;
		esferaJ1.centro.x=-6;
		esferaJ1.centro.y=((jugador1.y1+jugador1.y2)/2);
		esferaJ1.radio=0.25f;
	    }
	}
     }       
}

void CMundo::gestionaConexiones(){
  char cad[100];
  unsigned int pos=0;
  while(!finalizar){
	usleep(10);
		conexiones.push_back(socket_comunicacion_cliente.Accept());
		pos=conexiones.size() - 1;
		conexiones[pos].Receive(cad, sizeof(cad));
		printf("Conectado al servidor %s\n",cad);
		//sleep(10000);
  }
  
}

void CMundo::Init()
{
char cad[100];

//variable de fin de progarama
finalizar = false;

printf("Servidor en ejecución...\n");
	/*conexiones.push_back(new Socket);
	conexiones[0]->Connect( (char *) "127.0.0.1",4200); //COMPROBAR PUERTO
	conexiones[0]->InitServer("127.0.0.1",4200); //COMPROBAR PUERTO
	socket_comunicacion_cliente = conexiones[0]->Accept();
	socket_comunicacion_cliente.Receive(cad, sizeof(cad));
	printf("Conectado al servidor %s\n",cad);
	conexiones.push_back(new Socket);*/
	
	socket_comunicacion_cliente.InitServer((char *)"127.0.0.1",4200);
	
	
//FIFO logger
	path_fifo = "/tmp/logger";
	if( (fd_logger = open( path_fifo, O_WRONLY) ) < 0){
	    perror("**Error openning logger FIFO");
	    close(fd_logger);
	}
	else printf("--FIFO logger abierta correctamente.\n");
	
//Hilo recepción de teclas
	if ( pthread_create(&th_keys_id1, NULL, hilo_comandosJ1, this) != 0){
	      perror("Error creating thread");
	      pthread_cancel(th_keys_id1);
	}  
	if ( pthread_create(&th_keys_id2, NULL, hilo_comandosJ2, this) != 0){
	      perror("Error creating thread");
	      pthread_cancel(th_keys_id2);
	}  

//Hilo gestión de conexiones
	if ( pthread_create(&th_connect_id, NULL, hilo_conexiones, this) != 0){
	      perror("Error creating thread");
	      pthread_cancel(th_connect_id);
	}

///////////////////////////////////////////////
  ///INICIALIZACIÓN DE LOS DATOS DE MUNDO///
///////////////////////////////////////////////


//datos del jugador y puntos
	datos1[0]=1;	 
	datos1[1]=0;
	datos2[0]=2;
	datos2[1]=0;
	Plano p;
	listaEsferas.push_back(new Esfera);

//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

//fondos dcho e izq
	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

//raqueta izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

//raqueta dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
//Tratamiento de señales
	  struct sigaction act;
	  act.sa_handler = manejadorSenales;
	  act.sa_flags=0;
	  sigemptyset(&act.sa_mask);
	  sigaction(SIGINT,&act,NULL);
	  sigaction(SIGTERM,&act,NULL);
	  sigaction(SIGPIPE,&act,NULL);
	  sigaction(SIGUSR1,&act,NULL);
}
